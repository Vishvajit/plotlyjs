import flask
from flask import request
DEBUG = True
app = flask.Flask(__name__)
app.config.from_object(__name__)

@app.route('/')
def any1():
    return flask.render_template('demo.html')


@app.route('/excel')
def index():
    return flask.render_template('excel.html')

@app.route('/bubble')
def bubble():
    return flask.render_template('site_demo_map_bubble.html')

@app.route('/bar_demo')
def bar_demo():
    return flask.render_template('bar_demo.html')

if __name__ == '__main__':
    app.run(port=5000)
