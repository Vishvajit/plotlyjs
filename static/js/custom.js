var mapModule = angular.module ("mapModule", []);

mapModule.config(['$interpolateProvider', function($interpolateProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
    }
]);

mapModule.controller ("UsMapController", [ "$scope", "$http", function ($scope, $http) {
    console.log('got the controller..')
});
