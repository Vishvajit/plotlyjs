
Plotly.d3.csv('/static/data/customer_analysis_data.csv', function(err, rows){

      function unpack(rows, key) {
          return rows.map(function(row) { return row[key]; });
      }

  var cities = unpack(rows, 'City'),
      customer_segments = unpack(rows, 'Customer Segment'),
      departments = unpack(rows, 'Department'),
      customer_names = unpack(rows, 'Customer Name'),
      states = unpack(rows, 'State'),
      sales = unpack(rows, 'Sales'),
      profits = unpack(rows, 'Profit'),
      regions = unpack(rows, 'Region'),
      customer_segment = unpack(rows, 'Customer Segment'),
      order_quantities = unpack(rows, 'Order Quantity'),

      scale = 50000,
      text_list = [],
      unique_regions = [],
      total_sales = [],
      total_order_quantities = [],
      total_profits = [];
      total_profit_ratio = [];
      unique_customer_names = []
      unique_customer_names_counts = []
      sales_per_customer = [];

for ( var i = 0 ; i < regions.length; i++) {
      if(unique_regions.indexOf(regions[i])==-1)
        unique_regions.push(regions[i]);
  }

for ( var i = 0 ; i < unique_regions.length; i++) {
      sales_per_customer[i] = unique_customer_names_counts[i] = total_sales[i] = total_order_quantities[i] = total_profits[i] = total_profit_ratio[i] = 0;
    }


for ( var i = 0 ; i < sales.length; i++) {
      region_index = unique_regions.indexOf(regions[i]);
      total_sales[region_index] += parseInt(sales[i]);
      total_order_quantities[region_index] += parseInt(order_quantities[i]);
      total_profits[region_index] += parseInt(profits[i]);

      if(unique_customer_names.indexOf(customer_names[i])==-1){
        unique_customer_names.push(customer_names[i]);
        unique_customer_names_counts[region_index] += 1;
      }
}

for ( var i = 0 ; i < unique_regions.length; i++) {
      total_profit_ratio[i] = (total_profits[i]/total_sales[i]);
      sales_per_customer[i] = (total_sales[i]/unique_customer_names_counts[i]);
    }

for (var i = 0 ; i < unique_regions.length; i++) {
      var currentText = "Sales: "+total_sales[i]+"\nOrder Quantities: "+total_order_quantities[i]+"\nProfit: "+total_profits[i]+"\nProfit Ratio:"+total_profit_ratio[i];
      text_list[i] = currentText;
    }

 var data = [{
    type: 'bar',
    x: total_sales,
    y: unique_regions,
    orientation: 'h',
    // text: text_list,
  }];
var layout = {
  title: 'Sales',
};
Plotly.newPlot('sales_div', data, layout);



 var data = [{
    type: 'bar',
    x: total_order_quantities,
    y: unique_regions,
    orientation: 'h'
  }];
var layout = {
  title: 'Order Quantity',
};
Plotly.newPlot('order_quantities', data, layout);




 var data = [{
    type: 'bar',
    x: total_profits,
    y: unique_regions,
    orientation: 'h'
}];
var layout = {
  title: 'Profit',
};
Plotly.newPlot('profits', data, layout);



 var data = [{
    type: 'bar',
    x: total_profit_ratio,
    y: unique_regions,
    orientation: 'h'
  }];

var layout = {
  title: 'Profit Ratio',
};

Plotly.newPlot('profit_ratio', data, layout);


 var data = [{
    type: 'bar',
    x: unique_customer_names_counts,
    y: unique_regions,
    orientation: 'h',
    // text: text_list,
  }];
var layout = {
  title: 'Customer Count',
};
Plotly.newPlot('unique_customer_names', data, layout);


 var data = [{
    type: 'bar',
    x: sales_per_customer,
    y: unique_regions,
    orientation: 'h',
    // text: text_list,
  }];
var layout = {
  title: 'Sales Per Customer',
};
Plotly.newPlot('sales_per_customer', data, layout);




});
