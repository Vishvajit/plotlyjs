Plotly.d3.csv('/static/data/data.csv', function(err, rows){

      function unpack(rows, key) {
          return rows.map(function(row) { return row[key]; });
      }

  var cityName = unpack(rows, 'City'),
      orderDate = unpack(rows, 'Order Date'),
      department = unpack(rows, 'Department'),
      category = unpack(rows, 'Category'),
      state = unpack(rows, 'State'),
      sales = unpack(rows, 'Sales'),
      profit = unpack(rows, 'Profit'),
      region = unpack(rows, 'Region'),
      customer_segment = unpack(rows, 'Customer Segment'),

      color = [,"rgb(255,65,54)","rgb(133,20,75)","rgb(255,133,27)","lightgrey"],
      hoverText = [],
      bubble_size = [],
      scale = 50000;


for ( var i = 0 ; i < cityName.length; i++) {
      if(hoverText.indexOf(cityName[i])==-1)
        hoverText.push(cityName[i]);
        bubble_size.push(profit[i])
}

console.log(hoverText);

//   var data = [{
//     name: 'scattered',
//      type: 'scattergeo',
//      locationmode: 'USA-states',
//      locations: cityName,
//      text: cityName,
//      hoverinfo: 'text',
     // marker: {
       // size: citySize,
//        line: {
//          color: 'black',
//          width: 2
//      },

//    }
// }];


var data = [{
        type: 'scattergeo',
        mode: 'markers',
        locations: hoverText,
        marker: {
            size: bubble_size,
            // color: [10, 20, 40, 50],
            // cmin: 0,
            // cmax: 50,
            colorscale: 'Greens',
            colorbar: {
                title: 'Some rate',
                ticksuffix: '%',
                showticksuffix: 'last'
            },
            line: {
                color: 'black'
            }
        },
        name: 'Overview Data'
    }];

    var layout = {
        'geo': {
            'scope': 'usa',
            'resolution': 300
        }
    };

    Plotly.newPlot('myDiv', data, layout);


// var layout = {
//       title: 'Data Overview',
//       showlegend: false,
//       geo: {
//         scope: 'usa',
//         projection: {
//           type: 'albers usa'
//         },
//         showland: true,
//         landcolor: 'rgb(217, 217, 217)',
//         subunitwidth: 1,
//         countrywidth: 1,
//         subunitcolor: 'rgb(255,255,255)',
//         countrycolor: 'rgb(255,255,255)'
//     },
// };

// Plotly.plot(myDiv, data, layout, {showLink: false});
  });
